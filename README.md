# bomberman
Bomberman game as a semester project for Software engineering course at Jagiellonian University.

To run the game go to Project/ and unzip the Program.zip file. Now simply run Bomberman.exe and enjoy! :)

- Project/Dokumentacja_projektu contains whole documentation of the project.
- Project/Kod/Bomberman contains code of the game.

Made by Rafał Brożek, Julia Buchko, Kacper Schnetzer, Filip Sowa and Dominik Wołek.